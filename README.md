Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?
	The logger.properties file indicates that the console will only accept INFO
	log types whereas the file logging properties is set to "ALL" so that when logging
	to the file, the logger will print all levels of log types, so that even the "FINER"
	level will get printed to the logger.log file.
	
2.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
	This line comes from the JUnit library. The logger in JUnit gives this message to our logger that we have
	enabled the test. Logger is a singleton, meaning that JUnit is using the same logger that
	our program is using, which is why we see this.
	
3.  What does Assertions.assertThrows do?
	The function asserts that the executable that we send to the function will throw
	the exception of the type specified as the first parameter. For example, assertThrows(TimerException, Timer.timeMe(-1))
	asserts that the function timeMe() will throw the TimerException exception when we send it -1.
	
4.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)
		The serialVersionUID is an ID that is used to verify that when we serialize and deserialize an object,
		that both the serialized and deserialized version are the same object. If we don't have this,
		we can't confirm the object that was deserialized was the same as the one that was serialized.
    2.  Why do we need to override constructors?
		We override the constructors because if we didn't, we would not be able to construct 
		the TimerException class, as the only constructor available would be for the base
		Exception class, which constructs a normal Exception.
    3.  Why we did not override other Exception methods?
		We didn't override the other Exception methods because in those cases, we don't
		need separate functionality from the base Exception class in those methods. In the constructor,
		we had to override it in order to create a TimerException, but the base Exception functions that 
		we didn't override don't need to be overriden to be used by the child Exception class we made.
		
5.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)
	The static block in Timer.java is a block of code that is run one time when the Timer object
	is first initialized in the program. This block specifically ensures that the logger used by Timer
	has access to the correct configuration file before being used for the first time. After applying
	the config file to the logger in Timer for the first time, this static block isn't run again while
	the program is running because it is only necessary as a "setup" type of function.
	
6.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
	A Readme.md file is used to explain the repository that the readme is in. The format is
	the Markdown format, which is significant because websites such as BitBucket understand
	how to parse this format, this is why we use the .md file extension, which tells
	BitBucket that this file is using the Markdown format. It allows for the readme to be more 
	structured using this formatting than a typical text file.
	
7.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)
	The test was failing because it was throwing a NullPointerException, whereas the AssertThrows
	is asserting that the function should be throwing a TimerException out of the timeMe() function.
	It throws the NullPointerException because the finally{} block is executed after the try{} block,
	and in the finally{} block, we are trying to subtract timeNow from currentTimeMillis, and timeNow
	was set to null at the beginning of the function and never changed after that since our try{} block
	threw an exception.
	
8.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)
	The actual issue is that the try{} is throwing the TimerException, but since we have a finally{} block,
	that must be visited before we enter the catch{} block. Since the finally{} block throws a NullPointerException
	due to the subtraction of null, our TimerException is forgotten and effectively swallowed,
	meaning the catch{} block won't be run and the TimerException that we were expecting wasn't 
	thrown as it should have been.
	
9.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 
	In repo.
	
10.  Make a printScreen of your eclipse Maven test run, with console
	in repo.
	
11.  What category of Exceptions is TimerException and what is NullPointerException
	TimerException is a checked exception that is checked at compile-time. This means that
	the exception is expected to be caught in your code. NullPointerException is a run-time or unchecked
	exception that is not expected to be caught by your program. NullPointerException is not checked at
	compile-time.
	
12.  Push the updated/fixed source code to your own repository.

Repo link: https://bitbucket.org/Zac_Steudel/lab8steudel/src/master/